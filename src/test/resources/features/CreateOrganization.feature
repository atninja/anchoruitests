@tests
Feature: Create organization

  Scenario: Create organization

    When I log in as "admin"
    Then I should see a "DashBoard" page

    Given I create organization "testOrg"
    When I open organization with name "testOrg"
    Then I should see a "DashBoard" page

    When I create organization "childTestOrg" as child of "testOrg" organization
    When I create "stdUser" account in "testOrg" organization

    When I delete organization with name "testOrg"
    Then I should see a "DashBoard" page
