package steps;

import DTOs.Organization;
import DTOs.Organizations;
import DTOs.User;
import DTOs.Users;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import helpers.DTOHelper;
import org.junit.Assert;
import pages.*;
import pages.elements.LeftSideBar;
import pages.elements.MiddleNavBar;

public class StepDefinitions {

    private Users users = DTOHelper.getUsersFromConfig();
    private Organizations organizations = DTOHelper.getOrganizationsFromConfig();

    @When("^I log in as \"([^\"]*)\"$")
    public void iLogInAs(String userName) {
        User userForLogin = DTOHelper.getUserByName(users, userName);
        LoginPage loginPage = new LoginPage();
        loginPage.loginAs(userForLogin);
    }

    @Then("^I should see a \"([^\"]*)\" page$")
    public void iShouldSeeAPage(String pageName) throws Exception {
        Class<?> clazz = Class.forName("pages." + pageName + "Page");
        Object page = clazz.getConstructor().newInstance();
        Assert.assertTrue(((Page) page).isPageOpened());
    }

    @When("^I create organization \"([^\"]*)\"$")
    public void iCreateOrganization(String organizationName) throws Exception {
        Organization organization = DTOHelper.getOrganizationByName(organizations, organizationName);
        CreateOrganizationPage createOrganizationPage = LeftSideBar.clickCreateOrganizationBtn();
        Assert.assertTrue(createOrganizationPage.isPageOpened());
        Assert.assertTrue(
                createOrganizationPage
                        .fillOrgRequiredFields(organization)
                        .setPolicies(organization)
                        .createOrganization()
                        .checkThatOrgWasCreated(organization)
        );
    }

    @Then("^I open organization with name \"([^\"]*)\"$")
    public void iOpenOrganizationWithName(String organizationName) {
        LeftSideBar.getOrganizationsList().openOrganizationWithName(organizationName);
    }


    @Then("^I delete organization with name \"([^\"]*)\"$")
    public void iDeleteOrganizationWithName(String organizationName) {
        LeftSideBar.getOrganizationsList().openOrganizationWithName(organizationName);
        MiddleNavBar.clickOnSettingsTab().deleteOrganization();
    }

    @When("^I create organization \"([^\"]*)\" as child of \"([^\"]*)\" organization$")
    public void iCreateOrganizationAsChildOfOrganization(String childOrgName, String organizationName) throws Exception {
        LeftSideBar.getOrganizationsList().openOrganizationWithName(organizationName);
        Organization organization = DTOHelper.getOrganizationByName(organizations, childOrgName);
        CreateOrganizationPage createOrganizationPage = LeftSideBar.clickCreateOrganizationBtn();
        Assert.assertTrue(createOrganizationPage.isPageOpened());
        Assert.assertTrue(
                createOrganizationPage
                        .fillOrgRequiredFields(organization)
                        .setPolicies(organization)
                        .createOrganization()
                        .checkThatOrgWasCreated(organization)
        );
    }

    @When("^I create \"([^\"]*)\" account in \"([^\"]*)\" organization$")
    public void iCreateAnAccountInOrganization(String userName, String organizationName) {
        User user = DTOHelper.getUserByName(users, userName);
        LeftSideBar.getOrganizationsList().openOrganizationWithName(organizationName);
        AccountsPage accountsPage = MiddleNavBar.clickOnAccountsTab();
        CreateAccountPage createAccountPage = accountsPage.clickCreateAccountBtn();
        createAccountPage
                .fillAccountInformation(user)
                .createAccount();
        accountsPage.checkThatAccountExists(user.getName(), user.getName());
    }
}
