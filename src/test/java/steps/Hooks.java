package steps;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelectorMode;
import com.codeborne.selenide.Selenide;
import cucumber.api.java.Before;

import java.io.FileInputStream;
import java.util.Properties;

public class Hooks {
    @Before
    public void setUp() throws Exception{
        Configuration.selectorMode = SelectorMode.Sizzle;
        Properties properties = new Properties();
        properties.load(new FileInputStream("src/test/resources/run.properties"));
        Selenide.open(properties.getProperty("url"));
    }
}
