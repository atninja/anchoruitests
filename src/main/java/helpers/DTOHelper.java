package helpers;

import DTOs.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.google.common.collect.MoreCollectors.onlyElement;

public class DTOHelper {

    public static Users getUsersFromConfig() {
        final ObjectMapper mapper = new ObjectMapper(new YAMLFactory()); // jackson databind
        File usersConfig = new File("src/test/resources/entities/users.json");
        Users users = null;
        try {
            users = mapper.readValue(usersConfig, Users.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return users;
    }

    public static Organizations getOrganizationsFromConfig() {
        final ObjectMapper mapper = new ObjectMapper(new YAMLFactory()); // jackson databind
        File organizationsConfig = new File("src/test/resources/entities/organizations.json");
        Organizations organizations = null;
        try {
            organizations = mapper.readValue(organizationsConfig, Organizations.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return organizations;
    }

    public static Organization getOrganizationByName(Organizations organizations, String organizationName) {
        return organizations
                .getOrganizations()
                .stream()
                .filter(org -> org.getName().equals(organizationName))
                .collect(onlyElement());
    }

    public static User getUserByName(Users users, String userName) {
        return users
                .getUsers()
                .stream()
                .filter(org -> org.getName().equals(userName))
                .collect(onlyElement());
    }

    public static Map<String, String> getPoliciesOfOrg(Organization organization) {
        return organization
                .getPolicies()
                .stream()
                .collect(Collectors.toMap(Policy::getName, Policy::getValue));
    }

    public static Map<String, String> getUserSettings(User user){
        return user
                .getUserSettings()
                .stream()
                .collect(Collectors.toMap(UserSettings::getName, UserSettings::getValue));
    }
}
