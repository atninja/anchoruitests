package pages;

import DTOs.User;
import com.codeborne.selenide.SelenideElement;
import helpers.DTOHelper;

import java.util.Map;

import static com.codeborne.selenide.Selenide.$;

public class CreateAccountPage implements Page {

    private SelenideElement firstName = $("#first_name");
    private SelenideElement lastName = $("#last_name");
    private SelenideElement email = $("#email");
    private SelenideElement password = $("#password");
    private SelenideElement passwordConfirm = $("#confirm");
    private SelenideElement siteAdmin = $("#site_admin");
    private SelenideElement systemAdmin = $("#system_admin");
    private SelenideElement createBtn = $("#account_submit");

    public CreateAccountPage fillAccountInformation(User user){
        firstName.setValue(user.getName());
        lastName.setValue(user.getName());
        email.setValue(user.getEmail());
        password.setValue(user.getPassword());
        passwordConfirm.setValue(user.getPassword());
        Map userSettings = DTOHelper.getUserSettings(user);
        if (userSettings.get("site_admin") != null)
            siteAdmin.setSelected(Boolean.parseBoolean(userSettings.get("site_admin").toString()));
        if (userSettings.get("system_admin") != null)
            systemAdmin.setSelected(Boolean.parseBoolean(userSettings.get("system_admin").toString()));
        return this;
    }

    public AccountsPage createAccount(){
        createBtn.click();
        return new AccountsPage();
    }

    @Override
    public boolean isPageOpened() {
        return firstName.isDisplayed();
    }
}
