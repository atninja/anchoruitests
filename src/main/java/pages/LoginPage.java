package pages;

import DTOs.User;
import com.codeborne.selenide.*;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class LoginPage implements Page {
    private SelenideElement userName = $("#username");
    private SelenideElement password = $("#password");
    private SelenideElement logInBtn = $("[type='submit']");
    private SelenideElement errorMsg = $(".error");

    public LoginPage loginAs(User user) {
        userName.setValue(user.getEmail());
        password.setValue(user.getPassword());
        logInBtn.click();
        return this;
    }

    public LoginPage checkError() {
        errorMsg.shouldBe(visible);
        return this;
    }

    @Override
    public boolean isPageOpened() {
        return userName.isDisplayed();
    }
}
