package pages;

public interface Page {
   boolean isPageOpened();
}
