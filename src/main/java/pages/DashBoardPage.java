package pages;

import com.codeborne.selenide.SelenideElement;
import pages.elements.LeftSideBar;
import pages.elements.MiddleNavBar;
import pages.elements.OrganizationsList;

import static com.codeborne.selenide.Selenide.$;

public class DashBoardPage implements Page {

    private MiddleNavBar midNavBar = new MiddleNavBar();
    private OrganizationsList organizationsList = LeftSideBar.getOrganizationsList();
    private SelenideElement countsTable = $(".gadgetCounts");

    @Override
    public boolean isPageOpened() {
        return countsTable.isDisplayed();
    }


}
