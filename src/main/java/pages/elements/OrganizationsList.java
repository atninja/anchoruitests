package pages.elements;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$$;
import static com.google.common.collect.MoreCollectors.onlyElement;

public class OrganizationsList {
    private ElementsCollection listOfOrganizations = $$(".tree_node");

    public OrganizationsList openOrganizationWithName(String organizationName) {
        SelenideElement organization =
                listOfOrganizations
                        .stream()
                        .filter(org -> org.getText().trim().equals(organizationName))
                        .collect(onlyElement());
        organization.click();
        return this;
    }

    public boolean containsOrganization(String organizationName) {
        return listOfOrganizations
                .stream()
                .filter(org -> org.getText().trim().equals(organizationName))
                .collect(onlyElement())
                .isDisplayed();
    }
}
