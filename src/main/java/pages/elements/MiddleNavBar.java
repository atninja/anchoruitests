package pages.elements;

import com.codeborne.selenide.SelenideElement;
import pages.AccountsPage;
import pages.DashBoardPage;
import pages.OrganizationSettingsPage;

import static com.codeborne.selenide.Selenide.$;

public class MiddleNavBar {
    private static SelenideElement dashBoardTab = $("[title=\"Dashboard\"]");
    private static SelenideElement settingTab = $("[title=\"Organization Settings\"]");
    private static SelenideElement accountsTab = $("[title=\"Accounts\"]");

    public static DashBoardPage clickOnDashBoardTab() {
        dashBoardTab.click();
        return new DashBoardPage();
    }

    public static OrganizationSettingsPage clickOnSettingsTab() {
        settingTab.click();
        return new OrganizationSettingsPage();
    }

    public static AccountsPage clickOnAccountsTab(){
        accountsTab.click();
        return new AccountsPage();
    }
}
