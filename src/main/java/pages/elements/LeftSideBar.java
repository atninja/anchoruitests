package pages.elements;

import com.codeborne.selenide.SelenideElement;
import pages.CreateOrganizationPage;

import static com.codeborne.selenide.Selenide.$;

public class LeftSideBar {
    private static OrganizationsList organizationsList = new OrganizationsList();
    private static SelenideElement createOrganizationBtn = $("#btn-create-site");

    public static OrganizationsList getOrganizationsList() {
        return organizationsList;
    }

    public static CreateOrganizationPage clickCreateOrganizationBtn() throws Exception {
        if (createOrganizationBtn.isDisplayed()) createOrganizationBtn.click();
        else throw new Exception("Organization button is not visible.");
        return new CreateOrganizationPage();
    }
}
