package pages;

import DTOs.Organization;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import helpers.DTOHelper;
import pages.elements.LeftSideBar;
import pages.elements.OrganizationsList;

import java.util.Map;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class CreateOrganizationPage implements Page {
    private SelenideElement organizationName = $("#name");
    private SelenideElement organizationSlug = $("#slug");
    private SelenideElement organizationEmail = $("#email");
    private SelenideElement saveBtn = $("#policy_submit");
    private SelenideElement successFlash = $(".flash.success");
    private SelenideElement createOrgHeader = $(".iSettings:contains('Create an Organization')");
    private ElementsCollection policies = $$(".formRight :first-child");
    private OrganizationsList organizationsList = LeftSideBar.getOrganizationsList();


    public CreateOrganizationPage fillOrgRequiredFields(Organization organization) {
        organizationName.setValue(organization.getName());
        organizationSlug.clear();
        organizationSlug.setValue(organization.getSlug());
        organizationEmail.setValue(organization.getEmail());
        return this;
    }

    public CreateOrganizationPage setPolicies(Organization organization) {
        Map policiesOfOrg = DTOHelper.getPoliciesOfOrg(organization);
        policies.stream().filter(policy -> {
            if (policy.getAttribute("id") != null)
                return policiesOfOrg.keySet().contains(policy.getAttribute("id"));
            else return false;
        }).forEach(policy -> {
            if (!policy.getAttribute("type").equals("checkbox"))
                policy.setValue(policiesOfOrg.get(policy.getAttribute("id")).toString());
            else {
                policy.setSelected(Boolean.parseBoolean(policiesOfOrg.get(policy.getAttribute("id")).toString()));
            }
        });
        return this;
    }

    public CreateOrganizationPage createOrganization() {
        saveBtn.click();
        return this;
    }

    public boolean checkThatOrgWasCreated(Organization organization) {
        successFlash.shouldBe(visible);
        return organizationsList.containsOrganization(organization.getName());
    }

    @Override
    public boolean isPageOpened() {
        return createOrgHeader.isDisplayed();
    }
}
