package pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class OrganizationSettingsPage implements Page {
    private SelenideElement generalTab = $("[title=\"General\"]");
    private SelenideElement deleteTab = $("[title=\"Delete Organization\"]");
    private SelenideElement deleteOrgBtn = $("#delete_organization");
    private SelenideElement deleteBtn = $("span:contains(Permanently Delete)");

    public DashBoardPage deleteOrganization(){
        return this
                .openDeleteTab()
                .clickDeleteBtn()
                .confirmDeletion();
    }

    private OrganizationSettingsPage openDeleteTab(){
        deleteTab.click();
        return this;
    }

    private OrganizationSettingsPage clickDeleteBtn(){
        deleteOrgBtn.click();
        return this;
    }

    private DashBoardPage confirmDeletion(){
        deleteBtn.click();
        return new DashBoardPage();
    }

    @Override
    public boolean isPageOpened() {
        return generalTab.isDisplayed();
    }
}
