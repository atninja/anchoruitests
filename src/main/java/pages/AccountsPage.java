package pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class AccountsPage implements Page {
    private SelenideElement createAccountBtn = $("[title=\"Create Account\"]");

    public CreateAccountPage clickCreateAccountBtn(){
        createAccountBtn.click();
        return new CreateAccountPage();
    }

    public boolean checkThatAccountExists(String firstName, String lastName){
        String accountNameLocator = "a:contains('%s, %s')";
        accountNameLocator = String.format(accountNameLocator, firstName, lastName);
        SelenideElement account = $(accountNameLocator);
        return account.isDisplayed();
    }

    @Override
    public boolean isPageOpened() {
        return createAccountBtn.isDisplayed();
    }
}
